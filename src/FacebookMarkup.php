<?php

namespace Drupal\media_entity_facebook;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Render\MarkupTrait;

/**
 * Class FacebookMarkup.
 */
class FacebookMarkup implements MarkupInterface {

  use MarkupTrait;

}
